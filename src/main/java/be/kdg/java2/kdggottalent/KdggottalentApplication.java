package be.kdg.java2.kdggottalent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KdggottalentApplication {

    public static void main(String[] args) {
        SpringApplication.run(KdggottalentApplication.class, args);
    }

}
